package com.example.SpringBootDemo1;

//import


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloSpring {

    @RequestMapping()
    public String Home(){
        return "Home";
    }


    @RequestMapping("/hello")
    public String HelloSpring(){
        return "Hello Spring";
    }
}
